<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/upjvTheme/templates/page.html.twig */
class __TwigTemplate_3ec7fb382bf7c7a62607bed7e153e42c703a21cf3171cf757bbea140323112f1 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@bootstrap_barrio/layout/page.html.twig", "themes/upjvTheme/templates/page.html.twig", 1);
        $this->blocks = [
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 74];
        $filters = ["escape" => 105];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doGetParent(array $context)
    {
        return "@bootstrap_barrio/layout/page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 73
    public function block_head($context, array $blocks = [])
    {
        // line 74
        echo "    ";
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 75
            echo "        <div id=\"bandeau\">
            <a href=\"/\" title=\"Université de Picardie Jules Verne \" id=\"logo\">UPJV</a>
            ";
            // line 78
            echo "            <span>Master <span>MIAGE</span> Amiens</span>
            <ul id=\"reseaux_sociaux\"><li><!--
\t\t --><a href=\"https://www.u-picardie.fr/ent\">
                        <img src=\"https://www.u-picardie.fr/medias/photo/ent_1503647800087-png\" alt=\"ENT\" title=\"ENT\">
                    </a><!--
  --></li><li><!--
\t\t --><a href=\"https://webtv.u-picardie.fr\">
                        <img src=\"https://www.u-picardie.fr/medias/photo/sans-titre-1_1506679360695-png\" alt=\"UPJV TV\" title=\"UPJV TV\">
                    </a><!--
  --></li><li><!--
\t\t --><a href=\"https://www.facebook.com/Univ.de.Picardie.JV\">
                        <img src=\"https://www.u-picardie.fr/medias/photo/fb_1414659715553-png\" alt=\"Facebook\" title=\"Facebook\">
                    </a><!--
  --></li><li><!--
\t\t --><a href=\"http://twitter.com/upjv\">
                        <img src=\"https://www.u-picardie.fr/medias/photo/tw_1414659879422-png\" alt=\"Twitter\" title=\"Twitter\">
                    </a><!--
  --></li><li><!--
\t\t --><a href=\"https://www.u-picardie.fr/adminsite/webservices/export_rss.jsp?objet=actualite&amp;SELECTION=0007&amp;TRI_DATE=DATE_DESC&amp;CODE_RUBRIQUE=ACTUALITES&amp;LANGUE=0&amp;NOMBRE=10\">
                        <img src=\"https://www.u-picardie.fr/medias/photo/rss_1414659912311-png\" alt=\"RSS\" title=\"RSS\">
                    </a><!--
  --></li>
            </ul>
        </div>
    ";
        }
        // line 103
        echo "    <!--SOF Navigation Region-->
    ";
        // line 104
        if (($this->getAttribute(($context["page"] ?? null), "navigation", []) || $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", []))) {
            // line 105
            echo "    <nav";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["navbar_attributes"] ?? null)), "html", null, true);
            echo " >
      <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#CollapsingNavbar\" aria-controls=\"CollapsingNavbar\" aria-expanded=\"false\" aria-label=\"Toggle navigation\"><span class=\"navbar-toggler-icon\"><i class=\"fa fa-bars\" aria-hidden=\"true\"></i></span></button>
      <div class=\"collapse navbar-collapse\" id=\"CollapsingNavbar\">
        ";
            // line 108
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation", [])), "html", null, true);
            echo " 
      </div>
    </nav>
    ";
        }
        // line 112
        echo "    <!--EOF Navigation Region-->

  
    <!--SOF Upjv_Area Region-->
    ";
        // line 116
        if ($this->getAttribute(($context["page"] ?? null), "upjv_area", [])) {
            // line 117
            echo "          ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "upjv_area", [])), "html", null, true);
            echo "
    ";
        }
        // line 119
        echo "    <!--SOF upjv_area Region-->
    ";
    }

    // line 122
    public function block_content($context, array $blocks = [])
    {
        // line 123
        echo "        <div id=\"main\" class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\">
          ";
        // line 124
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "breadcrumb", [])), "html", null, true);
        echo "
          <div class=\"row row-offcanvas row-offcanvas-left clearfix\">
              <main";
        // line 126
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content_attributes"] ?? null)), "html", null, true);
        echo ">
                <section class=\"section\">
                  <a id=\"main-content\" tabindex=\"-1\"></a>
                  ";
        // line 129
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
                </section>
              </main>
            ";
        // line 132
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 133
            echo "              <div";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebar_first_attributes"] ?? null)), "html", null, true);
            echo ">
                <aside class=\"section\" role=\"complementary\">
                  ";
            // line 135
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
            echo "
                </aside>
              </div>
            ";
        }
        // line 139
        echo "            ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 140
            echo "                <div";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebar_second_attributes"] ?? null)), "html", null, true);
            echo ">
                <aside class=\"section\" role=\"complementary\">
                  ";
            // line 142
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
            echo "
                     ";
            // line 144
            echo "                </aside>
              </div>
            ";
        }
        // line 147
        echo "          </div>
        </div>
";
    }

    // line 151
    public function block_footer($context, array $blocks = [])
    {
        // line 152
        echo "        <div class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\">
          ";
        // line 153
        if (((($this->getAttribute(($context["page"] ?? null), "footer", []) || $this->getAttribute(($context["page"] ?? null), "footer_second", [])) || $this->getAttribute(($context["page"] ?? null), "footer_third", [])) || $this->getAttribute(($context["page"] ?? null), "footer_fourth", []))) {
            // line 154
            echo "              <div class=\"col\">
                  ";
            // line 155
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
            echo "
            </div>
          ";
        }
        // line 158
        echo "        </div>
";
    }

    public function getTemplateName()
    {
        return "themes/upjvTheme/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  224 => 158,  218 => 155,  215 => 154,  213 => 153,  208 => 152,  205 => 151,  199 => 147,  194 => 144,  190 => 142,  184 => 140,  181 => 139,  174 => 135,  168 => 133,  166 => 132,  160 => 129,  154 => 126,  149 => 124,  144 => 123,  141 => 122,  136 => 119,  130 => 117,  128 => 116,  122 => 112,  115 => 108,  108 => 105,  106 => 104,  103 => 103,  76 => 78,  72 => 75,  69 => 74,  66 => 73,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/upjvTheme/templates/page.html.twig", "/Applications/MAMP/htdocs/drupal2/themes/upjvTheme/templates/page.html.twig");
    }
}
